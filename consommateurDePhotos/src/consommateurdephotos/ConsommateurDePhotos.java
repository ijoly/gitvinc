/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package consommateurdephotos;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.border.LineBorder;
import ws.IOException_Exception;

/**
 *
 * @author rougelotugo
 */
public class ConsommateurDePhotos extends JFrame{

    private JFrame fr = new JFrame("Gestion des photos");
    private JFrame modal = new JFrame("Apercu des photos");
    private JPanel p = new JPanel();
    private JButton currentBtnRes = null;
    private String directory = "/Users/rougelotugo/Desktop/webService/download/";
    
    public List<String> image = liste();
    
    public ConsommateurDePhotos() throws IOException_Exception{
        fr.setLocation((Toolkit.getDefaultToolkit().getScreenSize().width  - getSize().width) / 2, (Toolkit.getDefaultToolkit().getScreenSize().height - getSize().height) / 2);
        fr.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        BoxLayout boxLayout = new BoxLayout(fr.getContentPane(), BoxLayout.Y_AXIS);
        fr.setLayout(boxLayout);
        //p.setLayout(new BoxLayout(p, BoxLayout.LINE_AXIS));
        displaybtnAdd(p);
        displayBtnDel(image);
        render();
    }
    
    private void setCurrentBtnRes(JButton cr){
        currentBtnRes = cr;
    }
    
    private JButton getCurrentBtnRes(){
        return currentBtnRes;
    }
     
    private void render(){
        fr.add(p);
        fr.pack();
        fr.setSize(1000, 800); 
        fr.setVisible(true);
    }
    
    private void displayBtnDel(List<String> s) throws IOException_Exception{
        for(String img : s)
        {
            displaySingleBtn(img);
        }
    }
    
    private void reload() throws IOException_Exception{
        System.out.println(image);
        System.out.println(image.size() - 1);
        displaySingleBtn(image.get(image.size() - 1));
        //displaySingleBtnRes(image.get(image.size() - 1));
        render();
    }
    
    private void displaySingleBtn(String img) throws IOException_Exception{
        JButton btnDel = new JButton("Supprimer "+img);
        //btnDel.setBorder(BorderFactory.createEmptyBorder(2,1,2,1));
        btnDel.setBackground(Color.red);
        btnDel.setPreferredSize(new Dimension(150, 150));
        
        //byte[] restitution = restituer(img);
        //ImageIcon rest = new ImageIcon(restitution);
        
        JButton btnRes = new JButton("Restituer "+img);
        btnRes.setBorder(BorderFactory.createEtchedBorder(0));
        btnRes.setBackground(Color.green);
        btnRes.setPreferredSize(new Dimension(150, 150));
        
        btnDel.setAlignmentX(Component.CENTER_ALIGNMENT);
        btnRes.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        btnRes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    System.out.println(directory + img);
                    byte[] restitution = restituer(img);
                    File fichier = new File(directory + img);
                    FileOutputStream photo = new FileOutputStream(fichier);
                    photo.write(restitution);
                    photo.close();
                    
                    ImageIcon rest = new ImageIcon(restitution);
                    fr.setIconImage(rest.getImage());

                } catch (IOException_Exception ex) {
                    Logger.getLogger(ConsommateurDePhotos.class.getName()).log(Level.SEVERE, null, ex);
                    System.out.println("Erreur de rest");
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(ConsommateurDePhotos.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(ConsommateurDePhotos.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        
        btnDel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("click");
                supprimer(img);
                
                p.remove(btnDel);
                p.remove(btnRes);
                p.revalidate();
                p.repaint();
            }
        });  
        p.add(btnDel);
        p.add(btnRes);
    }
    
    
    private void displaybtnAdd(JPanel p ){
                JButton b = new JButton("Ajouter Une image");
                b.setAlignmentX(Component.CENTER_ALIGNMENT);
                b.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    // get File and directory
                    File inputFile = new ChooseFile().getFile();
                    System.out.println("file selected : " + inputFile);
                    // get file name
                    String file = inputFile.getAbsolutePath();
                    String fileName = inputFile.getName();
                    System.out.println(inputFile.getName());

                    File f = new File(file); 
                    if(f.exists())
                    {
                        byte[] fileToByte = new byte[(int)f.length()];
                        try {
                            stocker(fileName , fileToByte);
                        } catch (IOException_Exception ex) {
                            Logger.getLogger(ConsommateurDePhotos.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }else{
                        System.out.println("Fichier �rron�s");
                    }
                    image.add(fileName);
                    try {
                        reload();
                    } catch (IOException_Exception ex) {
                        Logger.getLogger(ConsommateurDePhotos.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });  
        p.add(b);
    }
    
    

    private static java.util.List<java.lang.String> liste() {
        ws.WSPHOTO_Service service = new ws.WSPHOTO_Service();
        ws.WSPHOTO port = service.getWSPHOTOPort();
        return port.liste();
    }

    private static void stocker(java.lang.String arg0, byte[] arg1) throws IOException_Exception {
        ws.WSPHOTO_Service service = new ws.WSPHOTO_Service();
        ws.WSPHOTO port = service.getWSPHOTOPort();
        port.stocker(arg0, arg1);
    }

    private static byte[] restituer(java.lang.String arg0) throws IOException_Exception {
        ws.WSPHOTO_Service service = new ws.WSPHOTO_Service();
        ws.WSPHOTO port = service.getWSPHOTOPort();
        return port.restituer(arg0);
    }

    private static void supprimer(java.lang.String arg0) {
        ws.WSPHOTO_Service service = new ws.WSPHOTO_Service();
        ws.WSPHOTO port = service.getWSPHOTOPort();
        port.supprimer(arg0);
    }
}
