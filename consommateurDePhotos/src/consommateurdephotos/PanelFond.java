/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package consommateurdephotos;

import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 *
 * @author rougelotugo
 */
public class PanelFond extends JPanel
{
    Image image;
    public PanelFond(ImageIcon rest)
    {
        image=rest.getImage();
        //System.out.println(image.getClass().getName().toString());
    }

    @Override
    public void paintComponent(Graphics g)
    {
        g.drawImage (image, 0, 0, null); // elle doit etre avant
        super.paintComponent(g); // lui il s'occupe de redessiner les composant enfant.
    }
}