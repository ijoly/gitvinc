
package ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ListeResponse_QNAME = new QName("http://WS/", "listeResponse");
    private final static QName _Liste_QNAME = new QName("http://WS/", "liste");
    private final static QName _Supprimer_QNAME = new QName("http://WS/", "supprimer");
    private final static QName _SupprimerResponse_QNAME = new QName("http://WS/", "supprimerResponse");
    private final static QName _IOException_QNAME = new QName("http://WS/", "IOException");
    private final static QName _Stocker_QNAME = new QName("http://WS/", "stocker");
    private final static QName _StockerResponse_QNAME = new QName("http://WS/", "stockerResponse");
    private final static QName _RestituerResponse_QNAME = new QName("http://WS/", "restituerResponse");
    private final static QName _Restituer_QNAME = new QName("http://WS/", "restituer");
    private final static QName _RestituerResponseReturn_QNAME = new QName("", "return");
    private final static QName _StockerArg1_QNAME = new QName("", "arg1");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ListeResponse }
     * 
     */
    public ListeResponse createListeResponse() {
        return new ListeResponse();
    }

    /**
     * Create an instance of {@link Liste }
     * 
     */
    public Liste createListe() {
        return new Liste();
    }

    /**
     * Create an instance of {@link Supprimer }
     * 
     */
    public Supprimer createSupprimer() {
        return new Supprimer();
    }

    /**
     * Create an instance of {@link SupprimerResponse }
     * 
     */
    public SupprimerResponse createSupprimerResponse() {
        return new SupprimerResponse();
    }

    /**
     * Create an instance of {@link Stocker }
     * 
     */
    public Stocker createStocker() {
        return new Stocker();
    }

    /**
     * Create an instance of {@link StockerResponse }
     * 
     */
    public StockerResponse createStockerResponse() {
        return new StockerResponse();
    }

    /**
     * Create an instance of {@link IOException }
     * 
     */
    public IOException createIOException() {
        return new IOException();
    }

    /**
     * Create an instance of {@link RestituerResponse }
     * 
     */
    public RestituerResponse createRestituerResponse() {
        return new RestituerResponse();
    }

    /**
     * Create an instance of {@link Restituer }
     * 
     */
    public Restituer createRestituer() {
        return new Restituer();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WS/", name = "listeResponse")
    public JAXBElement<ListeResponse> createListeResponse(ListeResponse value) {
        return new JAXBElement<ListeResponse>(_ListeResponse_QNAME, ListeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Liste }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WS/", name = "liste")
    public JAXBElement<Liste> createListe(Liste value) {
        return new JAXBElement<Liste>(_Liste_QNAME, Liste.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Supprimer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WS/", name = "supprimer")
    public JAXBElement<Supprimer> createSupprimer(Supprimer value) {
        return new JAXBElement<Supprimer>(_Supprimer_QNAME, Supprimer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SupprimerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WS/", name = "supprimerResponse")
    public JAXBElement<SupprimerResponse> createSupprimerResponse(SupprimerResponse value) {
        return new JAXBElement<SupprimerResponse>(_SupprimerResponse_QNAME, SupprimerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IOException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WS/", name = "IOException")
    public JAXBElement<IOException> createIOException(IOException value) {
        return new JAXBElement<IOException>(_IOException_QNAME, IOException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Stocker }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WS/", name = "stocker")
    public JAXBElement<Stocker> createStocker(Stocker value) {
        return new JAXBElement<Stocker>(_Stocker_QNAME, Stocker.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StockerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WS/", name = "stockerResponse")
    public JAXBElement<StockerResponse> createStockerResponse(StockerResponse value) {
        return new JAXBElement<StockerResponse>(_StockerResponse_QNAME, StockerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RestituerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WS/", name = "restituerResponse")
    public JAXBElement<RestituerResponse> createRestituerResponse(RestituerResponse value) {
        return new JAXBElement<RestituerResponse>(_RestituerResponse_QNAME, RestituerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Restituer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WS/", name = "restituer")
    public JAXBElement<Restituer> createRestituer(Restituer value) {
        return new JAXBElement<Restituer>(_Restituer_QNAME, Restituer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "return", scope = RestituerResponse.class)
    public JAXBElement<byte[]> createRestituerResponseReturn(byte[] value) {
        return new JAXBElement<byte[]>(_RestituerResponseReturn_QNAME, byte[].class, RestituerResponse.class, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "arg1", scope = Stocker.class)
    public JAXBElement<byte[]> createStockerArg1(byte[] value) {
        return new JAXBElement<byte[]>(_StockerArg1_QNAME, byte[].class, Stocker.class, ((byte[]) value));
    }

}
