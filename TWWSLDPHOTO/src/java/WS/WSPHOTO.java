/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WS;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author Alexy
 */
@WebService(serviceName = "WSPHOTO")
@Stateless
public class WSPHOTO {

    private final String répertoire = "E:\\EPSI\\JAVA\\Archivage\\";

    /**
     * This is a sample web service operation
     */
    @Asynchronous
    public void stocker(String nom, byte[] octets) throws IOException {
        File fichier = new File(répertoire + nom);
        if (fichier.exists()) {
            return;
        }
        FileOutputStream photo = new FileOutputStream(fichier);
        photo.write(octets);
        photo.close();
    }

    public byte[] restituer(String nom) throws IOException {
        File fichier = new File(répertoire + nom);
        if (!fichier.exists()) {
            return null;
        }
        FileInputStream photo = new FileInputStream(fichier);
        byte[] octets = new byte[(int) fichier.length()];
        photo.read(octets);
        photo.close();
        return octets;
    }

    public String[] liste() {
        return new File(répertoire).list();
    }

    public void supprimer(String nom) {
        new File(répertoire + nom).delete();
    }

}
